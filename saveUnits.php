<?php
// Prettify JSON for Unit
$json = file_get_contents("php://input");
$data=json_decode($json);
if ($_GET["serv"] === NULL) {
    exit ("Parameter 'serv' not specified");
}
$serveur = $_GET["serv"];

$formatedJson = "[\n";
foreach($data as $val) {
    
    // loop over skills
    $skillLines = "[\n";
    foreach ($val->skills as $skill) {
        $skillLines .= "\t\t" . json_encode($skill, JSON_UNESCAPED_UNICODE) . ",\n";
    }
    $skillLines = rtrim($skillLines, ",\n");
    $skillLines .= "\n\t]},\n";

    // clear skills before json it 
    $val->skills = [];
    $formatedJson .=  "\t" . json_encode($val, JSON_UNESCAPED_UNICODE);
    // trim the last characters because skillLines add it
    $formatedJson = rtrim($formatedJson, "[]},");
    $formatedJson .= $skillLines;
}
$formatedJson = rtrim($formatedJson, ",\n");
$formatedJson .= "\n]";

// And finaly we save our json
file_put_contents("data/$serveur/units.json",$formatedJson);
?>