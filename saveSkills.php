<?php
// Prettify JSON a bit before saving it
$json = file_get_contents("php://input");
$data=json_decode($json);
if ($_GET["serv"] === NULL) {
    exit ("Parameter 'serv' not specified");
}
$serveur = $_GET["serv"];

$formatedJson = "[\n";
foreach($data as $val) {
    $formatedJson .=  "\t" . json_encode($val, JSON_UNESCAPED_UNICODE) . ",\n";
}
$formatedJson = rtrim($formatedJson, ",\n");
$formatedJson .= "\n]";
file_put_contents("data/$serveur/skills.json",$formatedJson);
?>