var skills = [];
var units = [];
var server = "GL";

// shortcut
var log = t => console.log(t);

// Sort unit's array by name
function sortUnit(a, b){
  var aName = a.name.toLowerCase();
  var bName = b.name.toLowerCase(); 
  return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
}

// Decode base64 to UTF8
function b64_to_utf8( str ) {
  return decodeURIComponent(escape(window.atob( str )));
}

// Encode UTF8 to base64
function utf8_to_b64( str ) {
  return window.btoa(unescape(encodeURIComponent( str )));
}

function switchTo(serv) {
  if (serv == "JP") {
    $(".switchServer .GL").removeClass("btn-primary").addClass("notSelected");
    $(".switchServer .JP").addClass("btn-primary").removeClass("notSelected");
    server = serv;
  } else if (serv == "GL") {
    $(".switchServer .GL").addClass("btn-primary").removeClass("notSelected");
    $(".switchServer .JP").removeClass("btn-primary").addClass("notSelected");
    server = serv;
  }
  populateSelectUnit();
  loadSkill();
}
// Fill unit dropdown 
function populateSelectUnit() {
  $.get("data/" + server + "/units.json", function(unitsResult) {
    unitsResult.sort(sortUnit);
    units = unitsResult;
    var options = $("#unitSelect");
    options.empty();
    options.append($("<option />").val("").text("Select a unit"));
    $.each(unitsResult, function() {
        options.append($("<option />").val(this.id).text(this.name));
    });
    $("#unitSelect").combobox("clearElement");
    $("#unitSelect").combobox("clearTarget");
    $("#unitSelect").combobox("refresh");
  }, 'json').fail(function(jqXHR, textStatus, errorThrown ) {
    console.log("get data/" + server + "/units.json : [" + textStatus + "] " + errorThrown);
  });
}

// Fill skill dropdown 
function populateSelectSkill(idUnit) {
  var unit = units.find(unit => unit.id == idUnit );
  // clear before
  var options = $("#skillSelect");
  options.empty();
  options.append($("<option />").val("").text("Select a skill"));
  $.each(unit.skills, function() {
    if (this.improved == 0)
      options.append($("<option />").val(this.id).text(this.name));
  });
  $('#skillSelect').combobox("enable");
  $("#skillSelect").combobox("refresh");
}

// Startup function : load skills
function loadSkill() {
  $.get("data/" + server + "/skills.json", function(result) {
    $.each(result, function() {
      skills.push(this);
  });
  }, 'json').fail(function(jqXHR, textStatus, errorThrown ) {
    console.log("get data/" + server + "/skills.json : [" + textStatus + "] " + errorThrown);
  });
}

// Search chaining partner by idSkill
function searchChainingPartners (idSkill) {
  // Get the selected skill
  var searchedSkill = skills.find(skill => skill.id == idSkill );
  if (searchedSkill == undefined) {
    return 
  }

  // Find unit already selected
  var IdSelectedUnit = $("#unitSelect").find(":selected").val();

  // Search unit with identical skills
  chaningPartners = [];
  for (var unit of units) {
    
    // Filter duplicate unit
    if (unit.id == IdSelectedUnit) {
      continue;
    }

    // List skill of unit and add the unit if a skill
    for (var skill of unit.skills) {
      if (skill.hits == searchedSkill.hits && skill.attack_frames[0].toString() == searchedSkill.attack_frames[0].toString()) { 
        chaningPartners.push({
          unit: unit,
          skill: skill
        });
        break;
      }
    }
  }
  displayChaningPartner(chaningPartners);
  return chaningPartners;
}

// Display chaining partner
// Format : [Unit rarity*] chaining skill
function displayChaningPartner(chaningPartners) {
  var html = "";
  if (chaningPartners.length == 0) {
    html += "<tr scope='row' ><td colspan='5' style='text-align: center;'>" +
      "<p>No partner found</p>" +
      "</td></tr>";
    $("#partnerTab tbody").html(html);
    return;
  }
  
  for (var partner of chaningPartners) {
    // log(partners);
    //html += "<br/>[" + partner.unit.name + " " + partner.unit.rarity + "*] " 
    //  + partner.skill.name + " (" + partner.skill.attack_type + " "  + partner.skill.type + " " + partner.skill.hits + " hits)";

    html += "<tr scope='row'>" +
    "<td style='vertical-align: middle'><div class='sprite'><img title='" + partner.unit.name + "' src='" + partner.unit.sprite + "' class='unit'></div></td>" +
    "<td>" +
    "  <div><img title='" + partner.skill.name + "' src='" + partner.skill.icon + "' class='icon'/></div>" +
    "  <div class='skill'>" + 
    "    <span class='name'>" + partner.skill.name + "</span>";
    for (var effect_desc of partner.skill.effects_desc) {
      html += "  <span class='effect'>" + effect_desc + "</span>";
    }
    html += "</div></td>" +
    "<td>" + partner.skill.hits + "</td>" + 
    "<td>" + partner.skill.attack_frames[0].sort((a,b) => a-b ).map((v, i, t) => v - ((i-1 == -1) ? 0 : t[i-1])).join('-') + "</td>" +
    "</tr>";
  }
  $("#partnerTab tbody").html(html);
  //$("#result").append(partners);
}
