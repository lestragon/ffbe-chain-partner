var rawSkills = null;
var rawUnits = null;
var rawEnhancements = null;
var updateServ = null;
var skillsById = [];

// Const for accessing the effect_raw attribute
var EFFECT_AREA = 0;
var EFFECT_TARGET = 1;
var EFFECT_TYPE = 2;
var EFFECT_DETAIL = 3;

// Const for type effect
var EFFECT_TYPE_RANDOM_SKILL = 29;
var EFFECT_TYPE_GRANT_DUAL_SKILL = 98;
var EFFECT_TYPE_SKILL_SWITCH = 99;
var EFFECT_TYPE_GRANT_SKILL = 100;

var TRIG_MIN_HIT = 3;

var ASSETS_UNITS =  "https://exvius.gg/static/img/assets/unit/unit_icon_";
var ASSETS_SKILLS = "https://exvius.gg/static/img/assets/ability/";

var REPO_GL = "https://api.github.com/repos/aEnigmatic/ffbe/contents/";
var REPO_JP = "https://api.github.com/repos/aEnigmatic/ffbe-jp/contents/";

function sortByName(a, b){
  var aName = a.name.toLowerCase();
  var bName = b.name.toLowerCase(); 
  return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
}

function logFailure (jqXHR, textStatus, errorThrown) {
  console.error("Error : " + errorThrown);
  console.error("jqXHR : " + jqXHR);
}

function multiHitFilter(val) {
  return val.hits >= TRIG_MIN_HIT && val.name != undefined;
}

function saveJSON(url, data) {
  $.ajax({
      url: url,
      method: 'PUT',
      data: JSON.stringify(data),
      dataType: "html",
      contentType: "application/json; charset=utf-8"
  })
  .done(function(jqXHR) { 
    $(".notification-text").html(updateServ + " data updated !").show();
  })
  .fail(logFailure);
}

// Entry point
// List and download unit, skill and enhancement files from aEnigmatic git
// Then call parseDump to keep only what's necessary
function getLatestDump(server) {
  // Get selected Serveur
  var dumpRepo = "";
  updateServ = server;
  if (server == "GL") {
    dumpRepo = REPO_GL;
  } else if (server == "JP") {
    dumpRepo = REPO_JP;
  } else {
    return 1;
  }

  // Get the list of files and their git_urls
  $.get(dumpRepo, function(files) {
    var gitUrls = [];
    for (var file of files) {
      if (file.name == "units.json" || file.name == "skills.json" || file.name == "enhancements.json") {
        gitUrls[file.name] = file._links.git;
      }
    }
    
    // Get units, skills and enhancements from git aEnigmatic dump
    console.log("Getting units...");
    $.get(gitUrls["units.json"], function(unitResult) {
      rawUnits = JSON.parse(b64_to_utf8(unitResult.content));
      console.log("Getting skills...");
      $.get(gitUrls["skills.json"], function(skillResult) {
        rawSkills = JSON.parse(b64_to_utf8(skillResult.content));
        console.log("Getting enhancements...");
        $.get(gitUrls["enhancements.json"], function(enhancementResult) {
          rawEnhancements = JSON.parse(b64_to_utf8(enhancementResult.content));
          // Now let's clean data
          console.log("Cleaning data");
          parseDump();          
        }, 'json').fail(logFailure);
      }, 'json').fail(logFailure);
    }, 'json').fail(logFailure);
  }, 'json').fail(function(jqXHR, textStatus, errorThrown ) {
    console.error("Unable to get file list from git. Error thrown : " + errorThrown);
  });
}

function getLocalDump() { 
  // Get units, skills and enhancements from tmp folder
  console.log("Getting units...");
  $.get("tmp/units.json", function(unitsResult) {
    console.log("Getting skills...");
    $.get("tmp/skills.json", function(skillResult) {
      console.log("Getting enhancements...");
      $.get("tmp/enhancements.json", function(enhancementResult) {
        console.log("Cleaning data");
        rawSkills = skillResult;
        rawUnits = unitsResult;
        rawEnhancements = enhancementResult;
        parseDump();          
      }, 'json').fail(logFailure);
    }, 'json').fail(logFailure);
  }, 'json').fail(logFailure);
}

function parseDump() {
  var skills = [];
  var units =  []; 

  // for every skills
  for (var sid in rawSkills) {
    var skill = {
      id: sid,
      name: rawSkills[sid].name,
      enhanced: getEnhanementLevel(sid),
      improved: 0,
      type: rawSkills[sid].type,
      icon: ASSETS_SKILLS + rawSkills[sid].icon,
      attack_type: rawSkills[sid].attack_type,
      hits: rawSkills[sid].attack_count[0],
      attack_frames: rawSkills[sid].attack_frames,
      attack_damage: rawSkills[sid].attack_damage,
      move_type: rawSkills[sid].move_type,
      motion_type: rawSkills[sid].motion_type,
      effects_raw: rawSkills[sid].effects_raw,
      effects_desc : rawSkills[sid].effects
    };
    
    skillsById[sid] = skill;
    skills.push(skill);
  }

  // Now get the units
  for (var uid in rawUnits) {
    var unit = rawUnits[uid];
    unit.id = uid; // add id field

    // Filter pot, gils snaper, etc.
    if (unit.skills == undefined 
      || unit.name == undefined
      || unit.name == "<na>") {
      continue;
    }
    
    var selectedSkill = getUnitSkill(unit);
    selectedSkill = selectedSkill.filter(multiHitFilter);
    // Only create unit with relevant skills
    if (selectedSkill.length == 0) continue; 
    units.push({
      id: uid,
      name: unit.name,
      rarity: unit.rarity_min,
      sprite: ASSETS_UNITS + Object.keys(unit.entries)[Object.keys(unit.entries).length - 1] + ".png",
      skills: selectedSkill
    });
  }

  units.sort(sortByName);
  skills.sort(sortByName);
  skills=skills.filter(multiHitFilter);

  // Save JSON on disk via PHP
  saveJSON('saveUnits.php?serv=' + updateServ, units);
  saveJSON('saveSkills.php?serv=' + updateServ, skills);
  //location.reload();
}

function getEnhanementLevel(sid) {
  for (var id in rawEnhancements) {
    if (rawEnhancements[id].skill_id_new == sid) {
      return 2 - (parseInt(id.substr(-1,1)) % 2)
    }
  }
  return 0;
}
function getUnitSkill (unit) {
  // Add enhancement in the unit skill array
  var unitSkills = [];
  for (var unitSkill of unit.skills) {
    unitSkills.push(skillsById[unitSkill.id]);

    var skillEnhancements = getEnhancements(unit.id,unitSkill.id);
    if (skillEnhancements.length == 0) continue; // Means there is no enhancement
    // Get +1 enhancement
    skillsById[skillEnhancements[0]].name += " +1";
    unitSkills.push(skillsById[skillEnhancements[0]]);
    // Get +2 enhancement
    skillsById[skillEnhancements[1]].name += " +2";
    unitSkills.push(skillsById[skillEnhancements[1]]);
  }

  // Add linkedSkill (for unit like prishe or trance terra)
  var tmpSkill = [];
  unitSkills.forEach(function (val){
    var t = getLinkedSkill(val);
    if (t.length > 0)
      tmpSkill = tmpSkill.concat(t);
  });

  // Do it twice because some units have recursivity in their skill (like Trance Terra)
  var tmpSkill2 = [];
  tmpSkill.forEach(function (val){
    var t = getLinkedSkill(val);
    if (t.length > 0)
    tmpSkill2 = tmpSkill2.concat(t);
  });

  if (tmpSkill.length > 0) unitSkills = unitSkills.concat(tmpSkill);
  if (tmpSkill2.length > 0) unitSkills = unitSkills.concat(tmpSkill2);

  // Add unit restriction skill
  var compendium_ids = [];
  for (var sid in rawSkills) {
    var rawSkill = rawSkills[sid];
    if (rawSkill.unit_restriction == null) continue;
    rawSkill.unit_restriction.forEach(function (uid){
      if (unit.id == uid && compendium_ids.indexOf(rawSkill.compendium_id) == -1 ) {
        unitSkills = unitSkills.concat(skillsById[sid]);
        compendium_ids.push(rawSkill.compendium_id)
      }
    });
  }
  return unitSkills;
}

function getEnhancements(idUnit, idSkill) {
  var tmpSkills = [];
  for (var i=1; i<9; i++) {
    idEnhancement = idSkill + "00" + i;
    // exit loop if there is no more enhancement for this skill
    if (rawEnhancements[idEnhancement] == undefined) break;
    // this enhancement doesnt concern this unit, check the next one for this skill
    if (rawEnhancements[idEnhancement].units.indexOf(parseInt(idUnit)) == -1) continue;
    // Now we're good
    tmpSkills.push(rawEnhancements[idEnhancement].skill_id_new);
  }
  return tmpSkills;
}

// Some example of the data structure
// Random Skill : [2, 1, 29, [[501420,  30], [501430,  30], [501440,  30], [501450,  10], 0]] ["Randomly use:\n30%: Tri-beam Laser (501420)\n30%: Tri-beam Laser (501430)\n30%: Tri-beam Laser (501440)\n10%: Tri-beam Laser (501450)"],
// Dual SKill : [0, 3, 98, [2, 219910, -1, [500580,  500590,  704490,  704500,  704670,  704680,  704520,  704690,  704700], 6, 1, 1]] (Gain 2 uses of the following skills for 5 turns:\n\tChaos Wave (500580, 500590)\n\tChaos Wave Awakened+2 (704520, 704690, 704700)\n\tChaos Wave+1 (704490, 704500)\n\tChaos Wave+2 (704670, 704680))
// Skill switch : [[0, 3, 99, [2,  209960,  2,  501240,  2,  501250]]] (Use Crushing Fist (501250), or Crushing Fist+ (501240), if used after Raging Fists (209960))
// Grant Skill : [[0, 3, 100, [2,  501540,  99999,  1,  1]]] (Gain access to following skills for one turn:\n\tFull Charge Stomp (501540))
function getLinkedSkill(skill) {
  var linkedSkills = [];
  if (! isContionalSkill(skill)) {
    return linkedSkills;
  }

  var enhancementText = (skill.enhanced != 0) ? " +" + skill.enhanced : "";
  
  for (var effect of skill.effects_raw) {
    var effectType = effect[EFFECT_TYPE];
    if (EFFECT_TYPE_RANDOM_SKILL == effectType) {
      // Only select the first skill
      var id = effect[EFFECT_DETAIL][0][0];
      var tmpSkill = skillsById[id];
      if (tmpSkill != undefined) {
        updateSkill(tmpSkill, enhancementText, skill.enhanced, 0);
        linkedSkills.push(tmpSkill);
      }
    } else if (EFFECT_TYPE_GRANT_DUAL_SKILL == effectType) {
      // @TODO
    } else if (EFFECT_TYPE_SKILL_SWITCH == effectType) {
      var improvedSkillId = effect[EFFECT_DETAIL][3];
      var normalSkillId = effect[EFFECT_DETAIL][5];

      if (Array.isArray(improvedSkillId)) {
        improvedSkillId.forEach(function(id) {
          updateSkill(skillsById[id], enhancementText, skill.enhanced, 1);
          linkedSkills.push(skillsById[id]);
        });
      } else {
        updateSkill(skillsById[improvedSkillId], enhancementText, skill.enhanced, 1);
        linkedSkills.push(skillsById[improvedSkillId]);
      }

      if (Array.isArray(normalSkillId)) {
        normalSkillId.forEach(function(id) {
          updateSkill(skillsById[id], enhancementText, skill.enhanced, 0);
          linkedSkills.push(skillsById[id]);
        })
      } else {
        updateSkill(skillsById[normalSkillId], enhancementText, skill.enhanced, 0);
        linkedSkills.push(skillsById[normalSkillId]);
      }
      // After getting linked skill, set base skill's hit number to 0 to delete duplicate entries
      skill.hits = 0;
    } else if (EFFECT_TYPE_GRANT_SKILL == effectType) {
      // effect[EFFECT_DETAIL][1] contain the skill unlocked
      if (Array.isArray(effect[EFFECT_DETAIL][1])) {
        for (var idSkill of effect[EFFECT_DETAIL][1]) {
          linkedSkills.push(skillsById[idSkill]);
        }
      } else {
        linkedSkills.push(skillsById[effect[EFFECT_DETAIL][1]]);
      }
    }
  } 
  return linkedSkills;
}

function updateSkill(skill, name, enhanced, improved) {
  skill.name += name;
  skill.enhanced = enhanced;
  skill.improved = improved;
}

// return true if skill give access to a new skill or is conditionnal
function isContionalSkill(skill) {
  var idx = skill.effects_raw.findIndex(function (e) {
    return e[EFFECT_TYPE] == EFFECT_TYPE_SKILL_SWITCH 
      || e[EFFECT_TYPE] == EFFECT_TYPE_GRANT_SKILL 
      || e[EFFECT_TYPE] == EFFECT_TYPE_RANDOM_SKILL
  });
  return idx >= 0;
}

